package steven.simpletools.mp;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;

/**
 * Created by dextry on 2017/4/19.
 */
public class MpTagsGet {
    public static void main(String[] args) throws UnirestException {
            Token token = new Token();
            HttpResponse<JsonNode> response = Unirest.get("https://api.weixin.qq.com/cgi-bin/groups/get")
                    .queryString("access_token", token.getValue()).asJson();
            System.out.println(response.getBody().toString());
        }

}
