package steven.simpletools.mp;

import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.commons.lang3.StringUtils;
import org.nutz.json.Json;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

/**
 * Created by theone on 2016/12/28.
 */
public class QuShuByConf {
    public static void main(String[] args) throws IOException, UnirestException, InterruptedException, ExecutionException, ConfigurationException {
        if (args.length == 0) {
            System.out.println("参数不正确。");
        }
        String path = args[0];
        if (path.equals("-check")) {
            DuplicatedDataCheck.main(new String[]{args[1]});
        } else if (path.equals("-untag")) {
            File file = new File(args[1]);
            if (file.isFile()) {
                Properties properties = new Properties();
                properties.load(new FileInputStream(file));
                String tagId = properties.getProperty("tagId");
                String lastTagFile = properties.getProperty("lastTagFile");
                //
                System.out.println("\n Renaming tagname as starting");
                MpTagRename.main(new String[]{tagId, "Process_" + System.currentTimeMillis()});
                //
                if (!new File(lastTagFile).exists()) {
                    System.out.println("\n Downloading and untag openid from MP");
                    MpDownloadAndUntag.main(new String[]{tagId, lastTagFile});
                }
            }
        } else if (path.equals("-rename")) {
            String tagId = args[1];
            String name = args[2];
            if (tagId == null) {
                System.out.println("请输入tagId");
            } else {
                MpTagRename.main(new String[]{tagId, name});
            }
        } else if (path.equals("-getFansById")) {
            if (args.length== 2){
                String tagId = args[1];
                    GetFansById.main(new String[]{tagId});
            }else{
                String tagId = args[1];
                String fans_path = args[2];

                    GetFansById.main(new String[]{tagId,fans_path});
            }

        } else if (path.equals("-createTag")) {

            String tagId = args[1];
            String name = args[2];
            if (tagId == null) {
                System.out.println("请输入tagId");
            } else {
                MpTagCreate.main(new String[]{tagId, name});
            }
        } else if (path.equals("-deleteTag")) {
            String tagId = args[1];
            if (tagId == null) {
                System.out.println("请输入tagId");
            } else {
                MpTagDelete.main(new String[]{tagId});
            }
        } else if (path.equals("-queryTags")) {
                MpTagsGet.main(args);

        } else if (path.equals("-removeDuplicates")) {
            String objectPath = args[1];
            String comparedPath = args[2];
            RemoveDuplicates.main(new String[]{objectPath, comparedPath});
        } else if (path.equals("-tag")) {
            File file = new File(args[1]);
            if (file.isFile()) {
                Properties properties = new Properties();
                properties.load(new FileInputStream(file));
                String tagId = properties.getProperty("tagId");
                String tagName = properties.getProperty("tagName");
                String tagFile = properties.getProperty("tagFile");
                String resultFile = properties.getProperty("resultFile");
                //
                System.out.println("\n Renaming tagname as starting");
                MpTagRename.main(new String[]{tagId, "Process_" + System.currentTimeMillis()});

                try {
                    System.out.println("Taging openid on MP");
                    MpUpload.main(new String[]{tagId, tagFile, "15", resultFile});
                } catch (EmptyStackException e) {
                    System.out.println("\n Renaming tagname as compeleted");
                    MpTagRename.main(new String[]{tagId, tagName});
                    System.out.println("\n\nTag DONE! " + tagName);
                    System.exit(0);
                }
            }
        } else if (path.equals("-mpnews")) {
            String mediaId = args[1];
            String num = args.length > 2 ? args[2] : "1";
            Mpnews.main(new String[]{mediaId, num});
        } else if (path.equals("-getData")) {
            File file = new File(args[1]);
            if (file.isFile() && file.getName().endsWith(".properties")) {
                Properties properties = new Properties();
                properties.load(new FileInputStream(file));
                String tmpTableResult = properties.getProperty("resultTable");
                String downloadPath = properties.getProperty("downloadPath");
                //
                String[] pamaters = new String[]{
                        tmpTableResult, downloadPath
                };
                System.out.println(Json.toJson(pamaters));
                Thread.sleep(10000);
                GetDateFromDB.main(pamaters);
            } else if (file.isFile() && file.getName().endsWith("xml")) {
                XMLConfiguration configuration = new XMLConfiguration(file);
                String tmpTableResult = configuration.getString("resultTable");
                String downloadPath = configuration.getString("downloadPath");
                //
                String[] pamaters = new String[]{
                        tmpTableResult, downloadPath
                };
                System.out.println(Json.toJson(pamaters));
                Thread.sleep(10000);
                GetDateFromDB.main(pamaters);
            } else {
                System.out.println("参数不正确，必须指定一个配置文件。");

            }

        } else {
            File file = new File(path);
            if (file.isFile() && file.getName().endsWith(".properties")) {
                Properties properties = new Properties();
                properties.load(new FileInputStream(file));
                String qrcodeIdQuerySQL = properties.getProperty("qrcodeIdQuerySQL");
                String whiteOpenIdList = properties.getProperty("whiteOpenIdList");
                String blackOpenIdList = properties.getProperty("blackOpenIdList");
                String blackTables = properties.getProperty("blackTables");
                String tmpTableResult = properties.getProperty("resultTable");
                if(tmpTableResult == null ){
                    System.out.println("参数不正确，请检查你的配置文件是否正确配置，或该配置文件是否适用该指令。");
                    System.exit(0);
                }
                String downloadPath = properties.getProperty("downloadPath");
                String exportSql = properties.getProperty("createResultTableSql");
                //
                String[] pamaters = new String[]{
                        qrcodeIdQuerySQL, whiteOpenIdList, blackOpenIdList, blackTables, tmpTableResult, downloadPath, exportSql
                };
                System.out.println(Json.toJson(pamaters));
                Thread.sleep(10000);
                QuShu.main(pamaters);
            } else if (file.isFile() && file.getName().endsWith("xml")) {
                XMLConfiguration configuration = new XMLConfiguration(file);
                String qrcodeIdQuerySQL = StringUtils.join(configuration.getStringArray("qrcodeIdQuerySQL"), ",");
                String whiteOpenIdList = StringUtils.join(configuration.getStringArray("whiteOpenIdList"), ",");
                String blackOpenIdList = StringUtils.join(configuration.getStringArray("blackOpenIdList"), ",");
                String blackTables = StringUtils.join(configuration.getStringArray("blackTables"), ",");
                String tmpTableResult = configuration.getString("resultTable");
                if(tmpTableResult == null ){
                    System.out.println("参数不正确，请检查你的配置文件是否正确配置，或该配置文件是否适用该指令。");
                    System.exit(0);
                }
                String downloadPath = configuration.getString("downloadPath");
                String exportSql = configuration.getString("createResultTableSql");
                //
                String[] pamaters = new String[]{
                        qrcodeIdQuerySQL, whiteOpenIdList, blackOpenIdList, blackTables, tmpTableResult, downloadPath, exportSql
                };
                System.out.println(Json.toJson(pamaters));
                Thread.sleep(10000);
                QuShu.main(pamaters);
            } else {
                System.out.println("参数不正确，必须指定一个配置文件。");
            }
        }
    }
}
