package steven.simpletools.mp;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;

/**
 * Created by theone on 2016/12/10.
 */
public class MpDownload {

    private int tagid;
    private String filepath;

    public MpDownload(String[] args) {
        this.tagid = Integer.valueOf(args[0]);
        this.filepath = args[1];
    }

    public static void main(String[] args) throws UnirestException, IOException, InterruptedException {
        MpDownload download = new MpDownload(args);
        download.run();
    }

    private void run() throws UnirestException, IOException, InterruptedException {
        Token token = new Token();
        JSONObject object = new JSONObject();
        object.put("tagid", this.tagid);
        String nextOpenId = "";

        File file = new File(this.filepath);
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        PrintWriter writer = new PrintWriter(new FileWriter(file));

        while (nextOpenId != null) {
            object.put("next_openid", nextOpenId);

            try {
                HttpResponse<JsonNode> response = Unirest.post("https://api.weixin.qq.com/cgi-bin/user/tag/get")
                        .queryString("access_token", token.getValue())
                        .body(object).asJson();
                //

                if (response.getBody().getObject().has("errcode")) {
                    if (response.getBody().getObject().getInt("errcode") == 40001) {
                        token.renew(token.getValue());
                        continue;
                    }
                }

                System.out.println(response.getBody().toString());
                int count = response.getBody().getObject().getInt("count");
                if (count == 0) {
                    return;
                }
                JSONArray array = response.getBody().getObject().getJSONObject("data").getJSONArray("openid");
                nextOpenId = response.getBody().getObject().getString("next_openid");

                System.out.println(nextOpenId);

                for (int i = 0; i < array.length(); i++) {
                    String openid = array.getString(i);
                    writer.println(openid);
                }
                writer.flush();

            } catch (Exception e) {

            }
            Thread.sleep(200);
        }

        writer.close();
    }
}
