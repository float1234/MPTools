package steven.simpletools.mp;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by theone on 2017/1/4.
 */
public class DuplicatedDataCheck {
    public static void main(String[] args) throws IOException {
        new DuplicatedDataCheck().check(new File(args[0]));
    }


    private void check(File dir) throws IOException {
        System.out.println(dir);
        Map<String, Integer> count = new HashMap();
        File[] files = dir.listFiles();
        System.out.println(count.size());
        for (File file : files) {
            if (file.isHidden()) {
                continue;
            }
            System.out.println(file);
            List<String> openIDs = FileUtils.readLines(file);
            System.out.println(openIDs.size());
            for (String openid : openIDs) {
                int c = count.containsKey(openid) ? count.get(openid) : 0;
                count.put(openid, c + 1);
            }
        }
        System.out.println(count.size());
        Set<Map.Entry<String, Integer>> set = count.entrySet();
        for (Map.Entry<String, Integer> e : set) {
            if (e.getValue() > 1) {
                System.out.println(e.getKey() + " " + e.getValue());
            }
        }
    }
}
