package steven.simpletools.mp;

import com.mysql.jdbc.SocketFactory;
import com.mysql.jdbc.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Executors;

/**
 * Created by theone on 2016/12/17.
 */
public class QuShu {
    private String qrcodeIdQuerySQL;
    private String whiteOpenIdList; //逗号分割的OpenId
    private String blackOpenIdList; //逗号分割的OpenId
    private String blackTables;
    private String tmpTableResult;
    private String resultSql;
    private String downloadPath;
   // private static final String URL = "jdbc:mysql://cnla20161207.mysqldb.chinacloudapi.cn:3306/cnla?autoReconnect=true";
    private static final String URL = "jdbc:mysql://127.0.0.1:3306/cnla?autoReconnect=true";
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    //private static final String USERNAME = "cnla20161207%db";
    private static final String USERNAME = "admin";
  //  private static final String PASSWORD = "FCOaYqm61jKQ";
    private static final String PASSWORD = "admin";
    private static Executors excutors ;
    private static Socket socket;
    private static Connection connection;




    static {
        try {
            Class.forName(DRIVER);
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public QuShu(String[] args) {

        this.qrcodeIdQuerySQL = args[0];
        this.whiteOpenIdList = args[1] == null ? "" : args[1];
        this.blackOpenIdList = args[2] == null ? "" : args[2];
        this.blackTables = args[3] == null ? "" : args[3];
        this.tmpTableResult = args[4];
        if (org.apache.commons.lang3.StringUtils.isBlank(this.tmpTableResult)) {
            throw new RuntimeException("tmpTableResult没有配置");
        }
        this.downloadPath = args[5];
        if (org.apache.commons.lang3.StringUtils.isBlank(this.downloadPath)) {
            throw new RuntimeException("downloadPath没有配置");
        }
        this.resultSql = args[6] == null ? "" : args[6];
        if (!new File(this.downloadPath).getParentFile().exists()) {
            throw new RuntimeException("下载目录不存在 " + this.downloadPath);
        }
    }

    public static void main(String[] args) {





        QuShu qushu = new QuShu(args);
        try {

            qushu.qushu();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void qushu() throws SQLException, IOException {
        long now = System.currentTimeMillis();
        //1. drop result table
        System.out.println("\n//1. drop result table " + this.tmpTableResult);
        this.dropResultTable();
        //2. create result table
        System.out.println("\n//2. create result table " + this.tmpTableResult);
        this.createResultTable();

        //3. black list
        System.out.println("\n//3. black list removed" + "\n");
        System.out.println(this.blackOpenIdList);
        this.blackList();
        //4. black table data
        System.out.println("\n//4. black list data from " + this.blackTables);
        this.deleteResultData();
        //5. white list
        System.out.println("\n//5. white list added");
        System.out.println(this.whiteOpenIdList);
        this.whiteList();
        //6. export data
        System.out.println("\n//6. export data to " + this.downloadPath);
        this.exportData();
        //DONE
        long sec = (System.currentTimeMillis() - now) / 1000;
        System.out.println("DONE: Total " + (int) (sec / 60) + "分钟" + (sec % 60) + "秒");
        System.exit(0);
    }

    private void deleteResultData() throws SQLException {
        if (this.blackTables != null && !StringUtils.isEmptyOrWhitespaceOnly(this.blackTables)) {
            List<String> tables = StringUtils.split(this.blackTables, ",", true);
            for (String table : tables) {
                String sql = "delete from " + this.tmpTableResult + " where openid in (select openid from " + table + ")";
                this.execute(sql);
            }
        }
    }

    private void exportData() throws SQLException, IOException {
        String sql = "select DISTINCT openid from " + this.tmpTableResult;
        System.out.println(Calendar.getInstance().getTime() + " - " + sql);
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(sql);

        File file = new File(this.downloadPath);
        if (file.exists()) {
            file.delete();
        }if(!file.exists()){
            file.createNewFile();
        }
        PrintWriter writer = new PrintWriter(new FileWriter(file));
        int buffer = 1000;
        int count = 0;
        while (rs.next()) {
            String openid = rs.getString(1);
            writer.println(openid);
            if (++count > 0 && count % buffer == 0) {
                System.out.println(Calendar.getInstance().getTime() + " - " + "write " + count);
                writer.flush();
            }
        }

        System.out.println(Calendar.getInstance().getTime() + " - " + "write " + count);
        writer.flush();
        writer.close();
        statement.close();
        System.out.println(Calendar.getInstance().getTime() + " - DONE");
    }

    private void createResultTable() throws SQLException {
        if (this.resultSql == null || StringUtils.isEmptyOrWhitespaceOnly(this.resultSql)) {
            String sql = "CREATE table " + this.tmpTableResult + " " +
                    "SELECT " +
                    "distinct fromusername as openid " +
                    "FROM " +
                    "wx_msg WHERE " +
                    "event = 'subscribe' " +
                    "AND eventkey IN " +
                    "( SELECT " +
                    "concat('qrscene_', wx_qrcode.sceneId) " +
                    "FROM " +
                    "wx_qrcode " +
                    "WHERE " +
                    "id in (" + this.qrcodeIdQuerySQL + ") " +
                    ") AND companyId = 20 and fromusername in (select openid from wx_mbr)";
            this.execute(sql);

        } else {
            List<String> sqls = StringUtils.split(this.resultSql, ";", true);
            for (String sql : sqls) {
                if (!StringUtils.isEmptyOrWhitespaceOnly(sql)) {
                    if (sql.toLowerCase().contains("drop")) {
                        try {
                            this.execute(sql);
                            continue;
                        } catch (Exception e) {
                            //
                        }
                    } else {
                        this.execute(sql);
                    }
                }
            }
        }

        String sql = "create  index idx_1 on " + this.tmpTableResult + " (openid);";
        this.execute(sql);
    }

    private void dropResultTable() {
        String sql = "drop table " + this.tmpTableResult;
        try {
            this.execute(sql);
        } catch (SQLException e) {
            //Ignored
        }
    }

    private void blackList() throws SQLException {
        List<String> openids = StringUtils.split(this.blackOpenIdList, ",", true);
        for (String openid : openids) {
            String sql = "delete from " + this.tmpTableResult + " where openid = '" + openid + "'";
            this.execute(sql);
        }
    }

    private void whiteList() throws SQLException {
        List<String> openids = StringUtils.split(this.whiteOpenIdList, ",", true);
        for (String openid : openids) {
            String sql = "insert into " + this.tmpTableResult + " (openid) values ('" + openid + "');";
            this.execute(sql);
        }
    }

    private void execute(String sql) throws SQLException {
        System.out.println(Calendar.getInstance().getTime() + " - " + sql);
        Statement statement = connection.createStatement();

       // statement.setQueryTimeout(6000);
        //statement.setFetchSize(1000);
       // statement.setPoolable(true);

        statement.execute(sql);
        statement.close();
    }
}
