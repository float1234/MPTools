package steven.simpletools.mp;

import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.*;

/**
 * Created by dextry on 2017/4/19.
 */
public class RemoveDuplicates {
    public static void main(String[] args) {
        File file = new File(args[0]);
        File file2 = new File(args[1]);
        BufferedReader br = null;
        Map<String, Integer> count = new HashMap();
        try {
            List<String> openIDs = FileUtils.readLines(file);
            List<String> openIDs2 = FileUtils.readLines(file2);
            System.out.println("original total " + openIDs.size());
            for (String openid : openIDs) {
                int c = count.containsKey(openid) ? count.get(openid) : 0;
                count.put(openid, c + 1);
            }
            for (String openid : openIDs2) {
                if (count.containsKey(openid)) {
                    count.put(openid, 0);
                }
            }


            Set<Map.Entry<String, Integer>> set = count.entrySet();
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            int number = 0;
            for (Map.Entry<String, Integer> e : set) {
                if (e.getValue() > 0) {
                    if (e.getValue() > 1) {
                        System.out.println(e.getKey() + " " + e.getValue());
                    }
                    bw.write(e.getKey());
                    bw.write("\r\n");
                    number++;
                }
            }
            System.out.println("write total " + number + " line");

            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
