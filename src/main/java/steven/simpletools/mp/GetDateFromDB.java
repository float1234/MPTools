package steven.simpletools.mp;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Calendar;
import java.util.concurrent.Executors;

/**
 * Created by dextry on 2017/1/17.
 */
public class GetDateFromDB {
  //  private static final String URL = "jdbc:mysql://cnla20161207.mysqldb.chinacloudapi.cn:3306/cnla?autoReconnect=true";
    private static final String URL = "jdbc:mysql://127.0.0.1:3306/cnla?autoReconnect=true";
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    // private static final String USERNAME = "cnla20161207%db";
    private static final String USERNAME = "admin";
    // private static final String PASSWORD = "FCOaYqm61jKQ";
    private static final String PASSWORD = "admin";
    private static  String tmpTableResult;
    private static  String downloadPath;
    private static Connection connection;
    private static Executors excutors ;

    static {
        try {
            Class.forName(DRIVER);
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            connection.setNetworkTimeout(excutors.newFixedThreadPool(1000), 5000000);
            //socket.setSoTimeout(3000000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        try {
            tmpTableResult = args[0];
            downloadPath = args[1];
            exportData(tmpTableResult, downloadPath);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private static  void exportData(String tmpTableResult,String downloadPath) throws SQLException, IOException {
        String sql = "select DISTINCT openid from " + tmpTableResult;
        System.out.println(Calendar.getInstance().getTime() + " - " + sql);
        Statement statement = connection.createStatement();
        statement.setFetchSize(1000);
        statement.setQueryTimeout(3000);
        ResultSet rs = statement.executeQuery(sql);
        rs.setFetchDirection(1000);

        File file = new File(downloadPath);
        if (file.exists()) {
            file.delete();
        }
        PrintWriter writer = new PrintWriter(new FileWriter(file));
        int buffer = 1000;
        int count = 0;
        while (rs.next()) {
            String openid = rs.getString(1);
            writer.println(openid);
            if (++count > 0 && count % buffer == 0) {
                System.out.println(Calendar.getInstance().getTime() + " - " + "write " + count);
                writer.flush();
            }
        }

        System.out.println(Calendar.getInstance().getTime() + " - " + "write " + count);
        writer.flush();
        writer.close();
        statement.close();
        System.out.println(Calendar.getInstance().getTime() + " - DONE");
        System.exit(0);
    }
}
