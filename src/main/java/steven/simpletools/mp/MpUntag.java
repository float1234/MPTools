package steven.simpletools.mp;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by theone on 2016/12/9.
 */
public class MpUntag {

    private int tagid;
    private File file;
    private int number;
    private ExecutorService service;

    public MpUntag(String[] args) {
        this.tagid = Integer.parseInt(args[0]);
        this.file = new File(args[1]);
        this.number = Integer.parseInt(args[2]);
        this.service = Executors.newFixedThreadPool(number);
    }

    public static void main(String[] args) throws UnirestException, InterruptedException, ExecutionException, IOException {
        MpUntag untag = new MpUntag(args);
        untag.run();
    }

    private void run() throws UnirestException, IOException, ExecutionException, InterruptedException {
        final Token token = new Token();
        //
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    HttpResponse<JsonNode> node = Unirest.get("https://api.weixin.qq.com/cgi-bin/tags/get")
                            .queryString("access_token", token.getValue())
                            .asJson();
                    JSONArray array = node.getBody().getObject().getJSONArray("tags");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        if (object.getInt("id") == tagid) {
                            if (object.getInt("count") == 0) {
                                System.out.println("\n\n Untag DONE!");
                                System.exit(0);
                            }
                            System.out.println(new Date() + ": " + object.get("name") + " >> " + object.get("count"));
                        }
                    }
                } catch (UnirestException e) {
                    e.printStackTrace();
                }
            }
        }, 0, 30000);
        //

        List<String> openIds = FileUtils.readLines(this.file);
        List<List<String>> list = ListUtils.partition(openIds, 50);
        List<Future> l = new ArrayList<>();
        for (List<String> totag : list) {
            Future f = service.submit(new UntagTask(tagid, totag, token));
            l.add(f);
        }
        for (Future f : l) {
            try {
                f.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        try {
            HttpResponse<JsonNode> node = Unirest.get("https://api.weixin.qq.com/cgi-bin/tags/get")
                    .queryString("access_token", token.getValue())
                    .asJson();
            JSONArray array = node.getBody().getObject().getJSONArray("tags");
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                if (object.getInt("id") == tagid) {
                    System.out.println(new Date() + ": " + object.get("name") + " >> " + object.get("count"));
                }
            }
        } catch (UnirestException e) {
        }
        System.exit(0);
    }
}
