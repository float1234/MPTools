package steven.simpletools.mp;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created by theone on 2016/12/9.
 */
public class MpUpload {

    private int tagid;
    private File file;
    private File resultFile;
    private int number;
    private PrintWriter printWriter;
    private ExecutorService service;

    public MpUpload(String[] args) throws IOException {
        this.tagid = Integer.parseInt(args[0]);
        this.file = new File(args[1]);
        this.number = Integer.parseInt(args[2]);
        this.resultFile = new File(args[3]);
        //this.service = Executors.newFixedThreadPool(number);
        //自定义队列数量，实现任务量大时队列自阻塞，避免空耗内存
        BlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(number);
        ThreadPoolExecutor executor = new ThreadPoolExecutor(number, number, 3, TimeUnit.HOURS, queue, new ThreadPoolExecutor.CallerRunsPolicy());
        this.service = executor;
    }


    public static void main(String[] args) throws UnirestException, IOException {
        MpUpload upload = new MpUpload(args);
        upload.run();
    }

    private void run() throws UnirestException, IOException {
        final Token token = new Token();
        //
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    int count = TagCount.getCount(tagid, token);
                    System.out.println(new Date() + ": " + count);
                } catch (UnirestException e) {
                    e.printStackTrace();
                }
            }
        }, 0, 30000);
        //
        int count = TagCount.getCount(tagid, token);
        if (count < 0) {
            System.exit(0);
        }
        if (count == 0 && this.resultFile.exists()) {
            this.resultFile.delete();
            this.resultFile.createNewFile();
        }

        this.printWriter = new PrintWriter(new FileWriter(this.resultFile, true));

        List<String> openIds = FileUtils.readLines(this.file);
        System.out.println("FileUtils.readLines(this.file)" + openIds.size());
        List<String> writedIds = FileUtils.readLines(this.resultFile);
        System.out.println("FileUtils.readLines(this.resultFile)" + writedIds.size());
        Set<String> openIdsSet = new HashSet<>(openIds);
        Set<String> writedIdsSet = new HashSet<>(writedIds);
        openIdsSet.removeAll(writedIdsSet);
        System.out.println("openIdsSet.removeAll(writedIdsSet);" + openIdsSet.size());
        if (openIdsSet.isEmpty()) {
           throw new EmptyStackException();
        }

        List<String> totags = new ArrayList<>(openIdsSet);
        System.out.println("ListUtils.partition(totags, 50)" + openIdsSet.size());
        List<List<String>> list = ListUtils.partition(totags, 50);
        List<Future> l = new ArrayList<>();
        for (List<String> totag : list) {
            Future f = service.submit(new TagTask(this, tagid, totag, token));
            l.add(f);
        }
        for (Future f : l) {
            try {
                f.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }
        System.out.println(TagCount.getCount(tagid, token));
        System.exit(0);
    }

    public void writeOK(List<String> totag) {
        for (String openid : totag) {
            this.printWriter.println(openid);
        }
        this.printWriter.flush();
    }
}
