package steven.simpletools.mp;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by theone on 2017/1/9.
 */
public class Mpnews {
    private Token token = new Token();

    public Mpnews() throws UnirestException {
    }

    public static void main(String[] args) throws UnirestException, JsonProcessingException {
        new Mpnews().copy(args[0], Integer.valueOf(args[1]));
    }

    private void copy(String mediaId, int num) throws UnirestException, JsonProcessingException {
        System.out.println("Find media..." + mediaId);
        List<News> newsList = this.find(mediaId, 0);
        if (newsList == null || newsList.isEmpty()) {
            throw new RuntimeException("MediaID不存在");
        }
        for (int i = 0; i < num; i++) {
            System.out.println("Upload new media...");
            this.upload(newsList);
        }
    }

    private void upload(List<News> newsList) throws UnirestException, JsonProcessingException {
        String url = "https://api.weixin.qq.com/cgi-bin/material/add_news";
        ObjectMapper mapper = new ObjectMapper();
        List<Map> list = new ArrayList<>();
        for (News news : newsList) {
            Map map = new HashMap();
            map.put("title", StringEscapeUtils.unescapeJava(news.title));
            map.put("content", StringEscapeUtils.unescapeJava(news.content));
            map.put("author", StringEscapeUtils.unescapeJava(news.author));
            map.put("content_source_url", StringEscapeUtils.unescapeJava(news.content_source_url));
            map.put("digest", StringEscapeUtils.unescapeJava(news.digest));
            map.put("thumb_media_id", StringEscapeUtils.unescapeJava(news.thumb_media_id));
            map.put("show_cover_pic", news.show_cover_pic);
            list.add(map);
        }
        Map map = new HashMap();
        map.put("articles", list);
        System.out.println(mapper.writeValueAsString(map));

        HttpResponse<JsonNode> response = Unirest.post(url).queryString("access_token", token.getValue()).body(mapper.writeValueAsString(map)).asJson();
        System.out.println(response.getBody().toString());
    }

    private List<News> find(String mediaId, int offset) throws UnirestException {
        String url = "https://api.weixin.qq.com/cgi-bin/material/batchget_material";

        JSONObject body = new JSONObject();
        body.put("type", "news");
        body.put("offset", offset);
        body.put("count", 20);
        HttpResponse<JsonNode> response = Unirest.post(url).queryString("access_token", token.getValue())
                .body(body).asJson();

        JSONObject obj = response.getBody().getObject();
        int total_count = obj.getInt("total_count");

        JSONArray items = obj.getJSONArray("item");
        for (int i = 0; i < items.length(); i++) {
            JSONObject item = items.getJSONObject(i);
            String media_id = item.getString("media_id");
            if (mediaId.equals(media_id)) {
                JSONObject content = item.getJSONObject("content");
                System.out.println(content.toString());
                JSONArray news_items = content.getJSONArray("news_item");
                List<News> newsList = new ArrayList<>();
                for (int j = 0; j < news_items.length(); j++) {
                    JSONObject news_item = news_items.getJSONObject(j);
                    News news = new News();
                    news.setAuthor(StringEscapeUtils.unescapeJava(news_item.getString("author")));
                    news.setContent(StringEscapeUtils.unescapeJava(news_item.getString("content")));
                    news.setContent_source_url(StringEscapeUtils.unescapeJava(news_item.getString("content_source_url")));
                    news.setDigest(StringEscapeUtils.unescapeJava(news_item.getString("digest")));
                    news.setShow_cover_pic(news_item.getInt("show_cover_pic"));
                    news.setThumb_media_id(StringEscapeUtils.unescapeJava(news_item.getString("thumb_media_id")));
                    news.setTitle(StringEscapeUtils.unescapeJava(news_item.getString("title")));
                    newsList.add(news);
                }
                return newsList;
            }
        }
        if (offset + 20 >= total_count) {
            return null;
        } else {
            return this.find(mediaId, offset + 20);
        }
    }

    private class News {
        private String title;
        private String thumb_media_id;
        private String author;
        private String digest;
        private int show_cover_pic;
        private String content;
        private String content_source_url;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getThumb_media_id() {
            return thumb_media_id;
        }

        public void setThumb_media_id(String thumb_media_id) {
            this.thumb_media_id = thumb_media_id;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getDigest() {
            return digest;
        }

        public void setDigest(String digest) {
            this.digest = digest;
        }

        public int getShow_cover_pic() {
            return show_cover_pic;
        }

        public void setShow_cover_pic(int show_cover_pic) {
            this.show_cover_pic = show_cover_pic;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getContent_source_url() {
            return content_source_url;
        }

        public void setContent_source_url(String content_source_url) {
            this.content_source_url = content_source_url;
        }
    }
}
