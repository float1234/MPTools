package steven.simpletools.mp;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by theone on 2017/1/11.
 */
public class TagCount {
    public static int getCount(int tagid, Token token) throws UnirestException {
        HttpResponse<JsonNode> node = Unirest.get("https://api.weixin.qq.com/cgi-bin/tags/get")
                .queryString("access_token", token.getValue())
                .asJson();
        JSONArray array = node.getBody().getObject().getJSONArray("tags");
        for (int i = 0; i < array.length(); i++) {
            JSONObject object = array.getJSONObject(i);
            if (object.getInt("id") == tagid) {
                System.out.println(new Date() + ": " + object.get("name") + " >> " + object.get("count"));
                return object.getInt("count");
            }
        }
        return -1;
    }
}
