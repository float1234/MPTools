package steven.simpletools.mp;


import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.body.RequestBodyEntity;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by theone on 2016/12/9.
 */
public class UntagTask implements Callable<List<String>> {

    private int tagid;
    private List<String> totag;
    private Token token;

    public UntagTask(int tagid, List<String> totag, Token token) {
        this.tagid = tagid;
        this.token = token;
        this.totag = totag;
    }

    @Override
    public List<String> call() {
        try {
            JSONObject object = new JSONObject();
            object.put("tagid", this.tagid);
            object.put("openid_list", totag);
            RequestBodyEntity body = Unirest.post("https://api.weixin.qq.com/cgi-bin/tags/members/batchuntagging")
                    .queryString("access_token", this.token.getValue()).body(object);
            HttpResponse<JsonNode> response =
                    body.asJson();
            int errorCode = response.getBody().getObject().getInt("errcode");
            if (errorCode != 0) {
                System.out.println("[Failed] " + Thread.currentThread().getName() + ", " + response.getBody().toString());
                if (errorCode == 40001) {
                    token.renew(token.getValue());
                    Unirest.post("https://api.weixin.qq.com/cgi-bin/tags/members/batchuntagging")
                            .queryString("access_token", this.token.getValue()).body(object);
                } else if (errorCode == -1) {
                }
                //Any error
                return this.totag;
            }

            //All is OK
            return new ArrayList<>();
        } catch (Exception e) {
            e.printStackTrace();
            return this.totag;
        }
    }
}
