package steven.simpletools.mp;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;

/**
 * Created by dextry on 2017/4/19.
 */
public class MpTagDelete {
    public static void main(String[] args) throws UnirestException {
            Token token = new Token();
            JSONObject object = new JSONObject();
            JSONObject tagid = new JSONObject();
            tagid.put("id", Integer.valueOf(args[0]));
            object.put("tag", tagid);
            HttpResponse<JsonNode> response = Unirest.post("https://api.weixin.qq.com/cgi-bin/tags/delete")
                    .queryString("access_token", token.getValue()).body(object).asJson();
            System.out.println(response.getBody().toString());
        }

}
