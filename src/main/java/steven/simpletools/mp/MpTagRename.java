package steven.simpletools.mp;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONArray;
import org.json.JSONObject;
import steven.simpletools.mp.tools.HttpClientUtil;

/**
 * Created by theone on 2016/12/18.
 */
public class MpTagRename {
    public static void main(String[] args) throws UnirestException {
        Token token = new Token();
        JSONObject object = new JSONObject();
        JSONObject tagid = new JSONObject();
        tagid.put("id", Integer.valueOf(args[0]));
        tagid.put("name", args[1]);
        object.put("tag", tagid);

        HttpResponse<JsonNode> response = Unirest.post("https://api.weixin.qq.com/cgi-bin/tags/update")
                .queryString("access_token", token.getValue()).body(object).asJson();
       // String response = HttpClientUtil.doPost("https://api.weixin.qq.com/cgi-bin/tags/update?access_token="+token.getValue(), object, "utf-8");


        System.out.println(response.getBody().toString());
        //System.out.println(response.toString());
    }
}
