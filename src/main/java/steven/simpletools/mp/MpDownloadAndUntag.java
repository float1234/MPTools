package steven.simpletools.mp;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by theone on 2016/12/10.
 */
public class MpDownloadAndUntag {

    private int tagid;
    private String filepath;
    private ExecutorService service;

    public MpDownloadAndUntag(String[] args) {
        this.tagid = Integer.valueOf(args[0]);
        this.filepath = args[1];
        //this.service = Executors.newFixedThreadPool(20);
        //自定义队列数量，实现任务量大时队列自阻塞，避免空耗内存
        BlockingQueue<Runnable> queue = new ArrayBlockingQueue<Runnable>(200);
        ThreadPoolExecutor executor = new ThreadPoolExecutor(200, 200, 3, TimeUnit.HOURS, queue, new ThreadPoolExecutor.CallerRunsPolicy());
        this.service = executor;
    }

    public static void main(String[] args) throws UnirestException, IOException, InterruptedException {
        MpDownloadAndUntag download = new MpDownloadAndUntag(args);
        download.run();
    }

    private void run() throws UnirestException, IOException, InterruptedException {
        Token token = new Token();
        JSONObject object = new JSONObject();
        object.put("tagid", this.tagid);
        String nextOpenId = "";

        File file = new File(this.filepath);
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        PrintWriter writer = new PrintWriter(new FileWriter(file));

        while (nextOpenId != null) {
            object.put("next_openid", nextOpenId);

            try {
                HttpResponse<JsonNode> response = Unirest.post("https://api.weixin.qq.com/cgi-bin/user/tag/get")
                        .queryString("access_token", token.getValue())
                        .body(object).asJson();
                //

                if (response.getBody().getObject().has("errcode")) {
                    if (response.getBody().getObject().getInt("errcode") == 40001) {
                        token.renew(token.getValue());
                        continue;
                    }
                }

                System.out.println(response.getBody().toString());
                int count = response.getBody().getObject().getInt("count");
                if (count == 0) {
                    return;
                }
                JSONArray array = response.getBody().getObject().getJSONObject("data").getJSONArray("openid");
                nextOpenId = response.getBody().getObject().getString("next_openid");

                System.out.println(nextOpenId);

                List<String> openids = new ArrayList<>();
                for (int i = 0; i < array.length(); i++) {
                    String openid = array.getString(i);
                    writer.println(openid);
                    //
                    openids.add(openid);
                }
                writer.flush();
                //
                List<List<String>> listList = ListUtils.partition(openids, 50);
                List<Future> l = new ArrayList<>();
                for (List<String> totag : listList) {
                    Future f = service.submit(new UntagTask(tagid, totag, token));
                    l.add(f);
                }
                for (Future f : l) {
                    try {
                        f.get();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }

            } catch (Exception e) {

            }
            Thread.sleep(200);
        }

        writer.close();
    }
}
