package steven.simpletools.mp;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 * Created by theone on 2016/12/9.
 */
public class Token {
    private String value;

    public Token() throws UnirestException {
        this.newToken();
    }

    public synchronized String getValue() {
        return this.value;
    }

    public synchronized void renew(String oldValue) throws UnirestException {
        if (this.value.equals(oldValue)) {
            this.newToken();
        }
    }

    private void newToken() throws UnirestException {
      //生活家
     //   HttpResponse<JsonNode> response = Unirest.get("http://pgwxaclprod.chinacloudapp.cn/acl/api/token?brandId=5d0b6dbb6bf82a6912528f79b327751c&nonce=4de754248c196c85ee4fbdcee89179bd&timestamp=1481214763&signature=5f82cf6c733c3761b75e77cde010d277baf8a132").asJson();
       //skii
      //  HttpResponse<JsonNode> response = Unirest.get("http://acl.pg.com.cn/acl/api/token?brandId=37870250e9f7872dea6a834439c5ff35&signature=53f5de7412a43195d22e33b5b8b353a83200bfd9&timestamp=1508743209&nonce=34345").asJson();
        //pampers
        // http://acl.pg.com.cn/acl/api/token?brandId=874fb164b7f968c734d3ca2b1ca1c479&signature=0e6130af9c4a6eda8e0af5ec934a5ca0f577c33b&timestamp=1510224515&nonce=343534

        //oral_b
        HttpResponse<JsonNode> response = Unirest.get("http://acl.pg.com.cn/acl/api/token?brandId=4874176a08afeece3970865565ee28c3&signature=82c678d5fa732d951c2fe03729e8970e7b4221a5&timestamp=1508743673&nonce=4343").asJson();

        System.out.println(response.getBody().getObject().toString());
        this.value = response.getBody().getObject().getString("access_token");
        System.out.println(this.getValue());
    }
}
