package steven.simpletools.mp;


import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.body.MultipartBody;
import com.mashape.unirest.request.body.RequestBodyEntity;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * Created by theone on 2016/12/9.
 */
public class TagTask implements Callable<Integer> {

    private int tagid;
    private List<String> totag;
    private Token token;
    private MpUpload upload;

    public TagTask(MpUpload upload, int tagid, List<String> totag, Token token) {
        this.upload = upload;
        this.tagid = tagid;
        this.totag = totag;
        this.token = token;
    }

    @Override
    public Integer call() {
        try {
            JSONObject object = new JSONObject();
            object.put("tagid", this.tagid);
            object.put("openid_list", totag);
            RequestBodyEntity body = Unirest.post("https://api.weixin.qq.com/cgi-bin/tags/members/batchtagging")
                    .queryString("access_token", this.token.getValue()).body(object);
            HttpResponse<JsonNode> response =
                    body.asJson();
            int errorCode = response.getBody().getObject().getInt("errcode");
            if (errorCode != 0) {
                System.out.println("[Failed] " + Thread.currentThread().getName() + ", " + response.getBody().toString());
                if (errorCode == 40001) {
                    token.renew(token.getValue());
                } else if (errorCode == -1 || errorCode == 45011) {
                    Thread.sleep(60000);
                }
                //Any error
                return -1;
            }

            //All is OK
            this.upload.writeOK(this.totag);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}